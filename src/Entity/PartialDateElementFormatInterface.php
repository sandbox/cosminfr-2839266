<?php

namespace Drupal\partial_date\Entity;


/**
 * Provides an interface defining an element of Partial Date Format config entity
 * 
 * @author CosminFr
 */
interface PartialDateElementFormatInterface {
  /**
   * Properties for PartialDateElementFormat:
   *   - required component
   *   - format
   *   - separator
   */
  
  /**
   * @return string
   *   The required component, may be empty.
   */
  public function getRequiredComponent();

  /**
   * @param string
   *   The required component. May be any of partial date component keys:
   *     - year, month, day, hour, minute, second, timezone
   *     - NULL or empty string: no requirements = it will be displayed even for empty values
   *
   * @return $this
   */
  public function setRequiredComponent($key);
  
  /**
   * @return string
   *   The element format.
   */
  public function getFormat();

  /**
   * @param string
   *   See PHP manual for date format options. Should relate to the required component.
   *
   * @return $this
   */
  public function setFormat($format);
  
  /**
   * @return string
   *   The text to write after formatted value before any other elements.
   */
  public function getSeparator();

  /**
   * @param string
   *   The text to write after formatted value before any other elements.
   *
   * @return $this
   */
  public function setSeparator($sep);
  
  
  /**
   * Check if the partial date has the required component for this format element.
   *
   * @param array $item
   *   The values array of a partial date field.
   *
   * @return bool
   *   TRUE if the required element has data, FALSE if not.
   */
  public function hasValue($item);
  
  /**
   * Returns the formatted value for this particular element and partial date value.
   *
   * @param array $item
   *   The values array of a partial date field.
   *
   * @return string
   */
  public function getText($item);
  
}
