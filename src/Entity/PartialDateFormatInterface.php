<?php

namespace Drupal\partial_date\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining FormatType config entity
 * 
 * @author CosminFr
 */
interface PartialDateFormatInterface extends ConfigEntityInterface {
  //Add get/set methods for your configuration properties here.

  
}
