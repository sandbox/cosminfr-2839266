<?php

/*
 * Static "stand-alone" date related functions.
 * 
 * Note: It would be great to move these to the date_api.
 * 
 */

namespace Drupal\partial_date;

use Drupal\Component\Utility\Unicode;

/**
 * Description of DateTools
 *
 * @author CosminFr
 */
class DateTools {
  
  /**
   * Returns true, if given $year is a leap year.
   *
   * @param  integer $year
   * @return boolean true, if year is leap year
   */
  public static function isLeapYear($year) {
    if (empty($year) || !is_numeric($year)) {
      return FALSE;
    }
    if ($year < 1582) {
      // pre Gregorio XIII - 1582
      return $year % 4 == 0;
    } else {
      // post Gregorio XIII - 1582
      return (($year % 4 == 0) && ($year % 100 != 0)) || ($year % 400 == 0);
    }
  }

  /**
   * Maps out the valid month ranges for a given year.
   *
   * @param int $year
   * @return array
   *   Note, there is no array index.
   */
  public static function monthMatrix($year = NULL) {
    if ($year && static::isLeapYear($year)) {
      return array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    }
    return array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
  }
  
  public static function lastDayOfMonth($month, $year = NULL) {
    $matrix = self::monthMatrix($year);
    return $matrix[$month];
  }

  /**
   * Returns a translated array of month names.
   */
  public static function monthNames($month = NULL) {
    static $month_names;
    if (empty($month_names)) {
      $month_names = array(
        1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May',
        6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September',
        10 => 'October', 11 => 'November', 12 => 'December');

      foreach ($month_names as $key => $month_name) {
        $month_names[$key] = t($month_name, array(), array('context' => 'datetime'));
      }
    }
    if ($month) {
      return $month_names[$month];
    }
    return $month_names;
  }

  /**
   * Returns a translated array of short month names.
   */
  public static function monthAbbreviations($month) {
    static $month_names;
    if (empty($month_names)) {
      $month_names = array(
        1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun',
        7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec');
      foreach ($month_names as $key => $month_name) {
        $month_names[$key] = t($month_name, array(), array('context' => 'datetime'));
      }
    }
    if ($month) {
      return $month_names[$month];
    }
    return $month_names;
  }

  /**
   * Returns a translated array of weekday names.
   */
  public static function weekdayNames($week_day_number) {
    static $day_names;
    if (empty($day_names)) {
      $day_names = array(
        0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday',
        4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');

      foreach ($day_names as $key => $day_name) {
        $day_names[$key] = t($day_name, array(), array('context' => 'datetime'));
      }
    }
    if ($week_day_number) {
      return $day_names[$week_day_number];
    }
    return $day_names;
  }

  /**
   * Returns a translated array of weekday names.
   */
  public static function weekdayAbbreviations($week_day_number, $length = 3) {
    $name = STATIC::weekdayNames($week_day_number);
    if (Unicode::strlen($name) > $length) {
      return Unicode::substr($name, 0, $length);
    }
    return $name;
  }

  /**
   * Gets day of week, 0 = Sunday through 6 = Saturday.
   *
   * Pope Gregory removed 10 days - October 5 to October 14 - from the year 1582
   * and proclaimed that from that time onwards 3 days would be dropped from the
   * calendar every 400 years.
   *
   * Thursday, October 4, 1582 (Julian) was followed immediately by Friday,
   * October 15, 1582 (Gregorian).
   *
   * @see PEAR::Date_Calc
   */
  public static function dayOfWeek($year, $month, $day) {
    $greg_correction = 0;
    if ($year < 1582 || ($year == 1582 && ($month < 10 || ($month == 10 && $day < 15)))) {
      $greg_correction = 3;
    }

    if ($month > 2) {
      $month -= 2;
    }
    else {
      $month += 10;
      $year--;
    }

    $result = floor((13 * $month - 1) / 5) +
           $day + ($year % 100) +
           floor(($year % 100) / 4) +
           floor(($year / 100) / 4) - 2 *
           floor($year / 100) + 77 + $greg_correction;

    return $result - 7 * floor($result / 7);
  }

  /**
   * Returns a translated ordinal suffix for a given day of the month.
   */
  public static function ordinalSuffix($day) {
//    static $nf;
//    if (empty($nf)) {
//      $nf = new \NumberFormatter('en_US', \NumberFormatter::ORDINAL);
//    }
//    return $nf->format($day);
    if (empty($day) || !is_numeric($day) || $day < 0) {
      return '';
    }
    static $suffixes;
    if (empty($suffixes)) {
      $suffixes = array(
        'st' => t('st', array(), array('context' => 'datetime')),
        'nd' => t('nd', array(), array('context' => 'datetime')),
        'rd' => t('rd', array(), array('context' => 'datetime')),
        'th' => t('th', array(), array('context' => 'datetime')),
      );
    }
    switch ($day % 100) {
      case 11:
      case 12:
      case 13:
        return $suffixes['th'];
      default:
        switch ($day % 10) {
          case 1:
            return $suffixes['st'];
          case 2:
            return $suffixes['nd'];
          case 3:
            return $suffixes['rd'];
          default:
            return $suffixes['th'];
        }
    }
  }
  
  /**
   * Return the "animal sign" for the specified year.
   * 
   * @param type $year
   * @return string
   */
  public static function getChineseZodiac($year){
    switch ($year % 12) {
        case  0: return 'Monkey';  // Years 0, 12, 1200, 2004...
        case  1: return 'Rooster';
        case  2: return 'Dog';
        case  3: return 'Pig';
        case  4: return 'Rat';
        case  5: return 'Ox';
        case  6: return 'Tiger';
        case  7: return 'Rabbit';
        case  8: return 'Dragon';
        case  9: return 'Snake';
        case 10: return 'Horse';
        case 11: return 'Goat';
    }
  }


  /**
   * Encode date (and time) components to a double value.
   *
   * @param array $values
   *   The array of date & time components (year, month, ... seconds) to encode.
   * @param boolean $end
   *   Optional specification to use "end of range" for unspecified components.
   * @return double
   *   A real number to store and index. 
   * The integer part represents the year while the decimal part encodes the 
   * remaining components on two digits each in order of significance.
   */
  public static function encodeDate($values, $end = FALSE) {
    //ensure all expected components are in array (NULL if not set).
    $result = $values + PARTIAL_DATE_EMPTY;
    foreach ($result as $key => $value) {
      if (empty($value)) {
        $result[$key] = ($end && $key != 'year') ? 99 : 0;
      }
    }
    $date = $result['year'] . '.'
        . sprintf('%02s', $result['month'])   // 0 or 1-12
        . sprintf('%02s', $result['day'])     // 0 or 1-31
        . sprintf('%02s', $result['hour'])    // 0 or 1-24
        . sprintf('%02s', $result['minute'])  // 0 or 1-60
        . sprintf('%02s', $result['second'])  // 0 or 1-60
        . '3'; //force a "dummy" extra digit to avoid approximation issues
    return ((double) $date);
  }

  /**
   * Decode date and time components from a double value.
   *
   * @param double $value
   *   A real number to be decoded. 
   * @return array
   *   The array of date & time components (year, month, ... seconds).
   *   May contain NULL for components represented as "0" or inconsistent numbers (aka month > 12)
   */
  public static function decodeDate($value) {
    $result = PARTIAL_DATE_EMPTY;
    if (!is_numeric($value)) {
      return $result;
    }  
    foreach ($result as $key => $val) {
      $val = static::extractValue($value);
      switch ($key) {
        case 'year':
          $result[$key] = ($val == 0) ? NULL : $val;
          break;  
        case 'month':
          $result[$key] = ($val < 1 || $val > 12) ? NULL : $val;
          break;  
        case 'day':
          $result[$key] = ($val < 1 || $val > 31) ? NULL : $val;
          break;  
        case 'hour':
          $result[$key] = ($val < 1 || $val > 24) ? NULL : $val;
          break;  
        case 'minute':
        case 'second':
          $result[$key] = ($val < 1 || $val > 60) ? NULL : $val;
          break;  
      }
    }
    return $result;
  }
  
  /**
   * Extracts the significant part (integer) and prepares the next significant
   * part to consider (multiply the remainder by $nextFactor).
   */
  private static function extractValue(&$value, $nextFactor = 100) {
    $result = (int) $value;
    $value = round(abs($value - $result) * $nextFactor, 10);
    return $result;
  }

}
