<?php

/**
 * @file
 * Contains \Drupal\partial_date\Plugin\Field\FieldType\PartialDateTimeRange.
 */

namespace Drupal\partial_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'partial_date_range' field type.
 *
 * @FieldType(
 *   id = "partial_date_range",
 *   label = @Translation("Partial Date Range"),
 *   description = @Translation("This field stores and renders partial date ranges."),
 *   module = "partial_date",
 *   default_widget = "partial_date_widget",
 *   default_formatter = "partial_date_formatter",
 * )
 */
class PartialDateTimeRange extends PartialDateTime {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $minimum_components = $field_definition->getSetting('minimum_components');
    //parse parent's properties and add duplicate "*_to" if needed.
    foreach ($properties as $key => $data) {
      switch ($data->getTag()) {
        case 'timestamp' :
          $properties['timestamp_to'] = DataDefinition::create('float')
            ->setLabel(t('End timestamp'))
            ->setDescription('Contains the best approximation for end value of the partial date')
            ->setRequired(TRUE);
          break;
        case 'date' :
        case 'time' :
          $properties[$key . '_to'] = DataDefinition::create('integer')
            ->setLabel($data->getLabel() . t(' end '))
            ->setDescription(t('The ' . $data->getLabel() . ' for the finishing date component.'))
            ->setRequired($minimum_components['to_granularity_' . $key]);
          break;
      }
    }
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    $schema = parent::schema($field);
    $minimum_components = $field->getSetting('minimum_components');

    foreach ($schema['columns'] as $key => $oldColumn) {
      switch (_partial_date_component_tag($key)) {
        case 'timestamp':
          $column = $oldColumn;
          $column['description'] = 'The calculated timestamp for end date stored in UTC as a float for unlimited date range support.';
          break;
        case 'date':
        case 'time':
          $column = $oldColumn;
          $column['description'] = 'The ' . $label . ' for the finishing date component.';
          $column['not null'] = $minimum_components['to_granularity_' . $key];
          break;
      }
      $schema['columns'][$key . '_to'] = $column;
    }
    $schema['indexes']['by_end'] = ['timestamp_to'];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'year_to' => [
        'Range' => [
          'min' => PD2_YEAR_MIN,
          'max' => PD2_YEAR_MAX,
        ],
      ],
    ]);

    return $constraints;
  }
  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (!parent::isEmpty()) {
      return FALSE;
    }
    $value = $this->getValue();
    if (!empty($value['timestamp_to'])) {
      return FALSE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
    $this->set('timestamp_to', DateTools::encodeDate($this->getEndValue()));
  }

  /*
   * Fill any missing values from the other end of the range (if any).
   * Ex. if year=NULL, but year_to=2015, make year=2015 too 
   *   and viceversa, if year_to not set, but we have a year set.
   * Note: If both values are set (and different) stop the normalization for 
   * the rest of the components.
   * Ex. from 2015 Jan 15 to Jul
   * The year is assumed the same, but the day is not. 
   */
  public function normalizeValues(){
    $values = $this->getValue();
    foreach (PARTIAL_DATE_KEYS as $key){
      $keyTo = $key . '_to';
      if (!empty($values[$key]) && empty($values[$keyTo])) {
        $this->set($keyTo, $values[$key]);
      } elseif (!empty($values[$keyTo]) && empty($values[$key])) {
        $this->set($key, $values[$keyTo]);
      } elseif (!empty($values[$keyTo]) && !empty($values[$key]) && $values[$keyTo] != $values[$key]) {
        break;
      }
    }    
  }
  
  public function getEndValue() {
    $result = PARTIAL_DATE_EMPTY; //empty array of components
    $values = $this->getValue();
    foreach (PARTIAL_DATE_KEYS as $key) {
      $keyTo = $key . '_to';
      if (!empty($values[$keyTo])) {
        $result[$key] = $values[$keyto];
      }
    }
    return $result;
  }
  
  public function hasRange() {
    return TRUE;  //Returns false for "partial_date" field.
  }
  
  public function hasTime() {
    $type = $this->getFieldDefinition()->getSetting('datetime_type');
    return ($type == DateTimeItem::DATETIME_TYPE_DATETIME);
  }
  
  public function hasTimeValue() {
    if (!$this->hasTime()) {
      return FALSE;  //time values not allowed!
    }
    $values = $this->getValue();
    foreach (array('hour', 'minute', 'second') as $key) {
      if (!empty($values[$key])) {
        return TRUE;
      }
    }
    return FALSE;
  }
  
  
}
