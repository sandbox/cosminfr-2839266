<?php

/**
 * @file
 * Contains \Drupal\partial_date\Plugin\Field\FieldType\PartialDateTime.
 */
namespace Drupal\partial_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\partial_date\DateTools;
use Drupal\partial_date\PartialDateComponentInfo;

/**
 * Plugin implementation of the 'partial_date' field type.
 *
 * @FieldType(
 *   id = "partial_date",
 *   label = @Translation("Partial Date"),
 *   description = @Translation("This field stores and renders partial dates."),
 *   module = "partial_date",
 *   default_widget = "partial_date_widget",
 *   default_formatter = "partial_date_formatter",
 * )
 */
class PartialDateTime extends DateTimeItem {

  /**
   * Value for the 'storage_type' setting:
   *   - detail  - store all date (and time) components separately.
   *   - compact - encode all details in the timestamp - WARNING: lossy for very large years (as in over 10k)
   */
  const PARTIALDATE_STORAGE_TYPE_DETAIL  = 'detail';
  const PARTIALDATE_STORAGE_TYPE_COMPACT = 'compact';

  
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $components = partial_date_components();
//    \Drupal::logger('partial_date')->debug('PartialDateTime.propertyDefinitions (' . serialize($field_definition) .')');
    $storeDetails = ($field_definition->getSetting('storage_type') == static::PARTIALDATE_STORAGE_TYPE_DETAIL);
    $storeTime    = ($field_definition->getSetting('datetime_type') == DateTimeItem::DATETIME_TYPE_DATETIME);
    foreach ($components as $data) {
      switch ($data->getTag()) {
        case 'date' :
          $data->setComputed(!$storeDetails);
          break;
        case 'time' :
          $data->setComputed(!$storeDetails || !$storeTime);
          break;
        case 'timezone' :
          $data->setComputed(!$storeTime);
          break;
        default :
          $data->setComputed(FALSE);
          break;
      }
      if ($data->isComputed()) {
        $data->setClass('\Drupal\partial_date\PartialDateTimeComputed')
           ->setSetting('date source', 'timestamp');
        
      }
    }
    return $components;
  }

  /**
   * {@inheritdoc}
   * Equivalent of hook_field_schema().
   *
   * This module stores a dates in a string that represents the data that the user
   * entered and a float timestamp that represents the best guess for the date.
   *
   * After tossing up the options a number of times, I've taken the conservative
   * opinion of storing all date components separately rather than storing these
   * in a singular field.
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    $schema = array();
    $components = static::propertyDefinitions($field);
    foreach ($components as $key => $data) {
      if (!$data->isComputed()) {
        $schema['columns'][$key] = array(
          'type' => $data->dbType(),
          'description' => $data->getDescription(),
          //'sortable' => TRUE,
        );
        if ($data->dbType() == 'varchar') {
          $schema['columns'][$key]['length'] = $data->getSize();
        } else {
          $schema['columns'][$key]['size']   = $data->getSize();
        }
      }
    }
    $schema['columns']['timestamp']['sortable'] = TRUE;
    $schema['indexes']['timestamp'] = array('timestamp');
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $constraints[] = $constraint_manager->create('ComplexData', [
      'year' => [
        'Range' => [
          'min' => PD2_YEAR_MIN,
          'max' => PD2_YEAR_MAX,
        ],
      ],
    ]);

    return $constraints;
  }

  protected function deleteConfig($configName) {
    //$config = \Drupal::service('config.factory')->getEditable($configName);
    $config = \Drupal::configFactory()->getEditable($configName);
    if (isset($config)) {
      $config->delete();
    }
  }

  public function delete() {
    $this->deleteConfig('partial_date.settings');
    $this->deleteConfig('partial_date.format');
    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->getValue();
//    \Drupal::logger('partial_date')->debug('PartialDateTime.isEmpty: ' . serialize($value) );
    if (empty($value) || !is_array($value)) {
      return TRUE;
    }
    if (!empty($value['timestamp'])) {
      return FALSE;
    }
    if (!empty($value['txt_short'])) {
      return FALSE;
    }
    if (!empty($value['txt_long'])) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
//      'datetime_type' => 'datetime',  //already in parent
      'storage_type' => 'detail',
    ) + parent::defaultStorageSettings();
  }
  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'require_consistency' => FALSE,
      'minimum_components' => array(
        'year' => FALSE,
        'month' => FALSE,
        'day' => FALSE,
        'hour' => FALSE,
        'minute' => FALSE,
        'second' => FALSE,
      ),
    ) + parent::defaultFieldSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    //debug_only:  var_dump($settings);
    $elements = array();
    $elements['require_consistency'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require consistent values'),
      '#default_value' => !empty($settings['require_consistency']),
      '#description' => t('Check to enforce a consistent date. For example, if day component is set, month (and year) are required too.'),
    );
    $elements['minimum_components'] = array(
      '#type' => 'partial_date_components_element',
      '#title' => t('Minimum components'),
      '#default_value' => $settings['minimum_components'],
      '#description' => t('These are used to determine if the field is incomplete during validation.'),
       //dynamically show/hide time components using javascript (based on another form element)
      '#time_states' =>  array(
          'visible' => array(
            ':input[id="has_time"]' => array('checked' => TRUE),
          ),
        ),
    );
    return $elements;
  }

  /**
   * {@inheritdoc}
   * Parent has "Date Type" select. Add new "Storage Type" select.
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = parent::storageSettingsForm($form, $form_state, $has_data);
//    $elements['has_range'] = array(
//      '#type' => 'checkbox',
//      '#id' => 'has_range',
//      '#title' => t('Allow range specification'),
//      '#default_value' => !empty($settings['has_range']),
//      '#description' => t('Clear if not holding end values. Check to explicitely show end of range values.'),
//    );
    $elements['storage_type'] = array(
      '#type' => 'select',
      '#title' => t('Storage type'),
      '#description' => t('Choose detailed or compact storage type. Detalied, keeps separate values for year, month, ..., second. Compact version encodes all info into the timestamp.'),
      '#default_value' => $this->getSetting('storage_type'),
      '#options' => array(
        static::PARTIALDATE_STORAGE_TYPE_DETAIL => t('Detailed in separate fields'),
        static::PARTIALDATE_STORAGE_TYPE_COMPACT => t('Encoded in timestamp'),
      ),
      '#disabled' => $has_data,
    );
    return $elements;
  }
  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
    //Calculate timestamp
    $timestamp = DateTools::encodeDate($this->getStartValue());
    $this->set('timestamp', $timestamp);
//    \Drupal::logger('partial_date')->debug('PartialDateTime.preSave encoding ' . serialize($this->getValue()) . ' as ' . $timestamp);
  }
  
  public function getStartValue() {
    $result = array_intersect_key($this->getValue(), PARTIAL_DATE_EMPTY);
    if (count($result) == 0) {
      $result = DateTools::decodeDate($this->timestamp);
//      \Drupal::logger('partial_date')->debug('PartialDateTime.getStartValue decoding ' . $this->timestamp . ' as ' . serialize($result) );
    }
    return $result;
  }

  public function getEndValue() {
    return $this->getStartValue();
  }
  
  public function hasRange() {
    return FALSE;  //Will return true for "partial_date_range" field.
  }
  
  public function hasTime() {
    $type = $this->getSetting('datetime_type');
    return ($type == DateTimeItem::DATETIME_TYPE_DATETIME);
  }
  
  public function hasComponents() {
    return !empty($this->getSetting('has_components'));
  }
  
  public function hasTimeValue() {
    if (!$this->hasTime()) {
      return FALSE;  //time values not allowed!
    }
    $values = array_intersect_key($this->getValue(), PARTIAL_DATE_EMPTY_TIME);
    foreach ($values as $value) {
      if (!empty($value)) {
        return TRUE;
      }
    }
    return FALSE;
  }
  
  
}
