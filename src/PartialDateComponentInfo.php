<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\partial_date;

use Drupal\Core\TypedData\DataDefinition;

/**
 * Description of PartialDateComponentInfo
 *
 * @author CosminFr
 */
class PartialDateComponentInfo extends DataDefinition {

  /**
   * Returns the data type definition appropriate for use in database (schema)
   */
  public function dbType() {
    $type = $this->getDataType();
    switch ($type) {
      case 'integer' :
        return 'int';
      case 'string' :
        return 'varchar';
      case 'any' :
        return 'varchar';
      default:
        return $type;
    }
  }

  /**
   * Magic method in case a forced string conversion is needed.
   * @return String - Returns component's Label
   */
  public function __toString() {
    return $this->getLabel();
  }
  
  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    parent::setLabel($label);
    if (!isset($this->definition['description'])){
      $this->definition['description'] = t('The @label for the time component.', array('@label' => $label));
    }
    return $this;
  }

  public function getSize() {
    return !empty($this->definition['size']) ? $this->definition['size'] : 'big';
  }
  public function setSize($size) {
    $this->definition['size'] = $size;
    return $this;
  }
  
  public function getTag() {
    return !empty($this->definition['tag']) ? $this->definition['tag'] : '';
  }
  public function setTag($tag) {
    $this->definition['tag'] = $tag;
    return $this;
  }
  
  public function allowNull() {
    return !empty($this->definition['allowNull']);
  }
  public function setAllowNull($allowNull) {
    $this->definition['allowNull'] = $allowNull;
    return $this;
  }

}
