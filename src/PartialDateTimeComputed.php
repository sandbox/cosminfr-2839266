<?php

namespace Drupal\partial_date;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedData;
use Drupal\partial_date\DateTools;

/**
 * A computed property for dates of date time field items.
 *
 * Required settings (below the definition's 'settings' key) are:
 *  - date source: The "timestamp" property containing the encoded partial date.
 */
class PartialDateTimeComputed extends TypedData {

  /**
   * Cached computed date.
   *
   * @var \DateTime|null
   */
  protected $components = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    if (!isset($name)) {
      $name = strtolower($definition->getLabel());
      \Drupal::logger('partial_date')->debug('PartialDateTimeComputed: setting property name:' . $name);
    }
    parent::__construct($definition, $name, $parent);
    if (!$definition->getSetting('date source')) {
      throw new \InvalidArgumentException("The definition's 'date source' key has to specify the name of the date property to be computed.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($langcode = NULL) {
    if (!isset($this->components) || !is_array($this->components)) {
      try {
        /** @var \Drupal\Core\Field\FieldItemInterface $item */
        $item  = $this->getParent();
        $value = $item->{($this->definition->getSetting('date source'))};

        \Drupal::logger('partial_date')->debug('PartialDateTimeComputed: calc from ' . $value);
    //    $datetime_type = $item->getFieldDefinition()->getSetting('datetime_type');
    //    $storage_format = $datetime_type === DateTimeItem::DATETIME_TYPE_DATE ? DATETIME_DATE_STORAGE_FORMAT : DATETIME_DATETIME_STORAGE_FORMAT;
        $this->components = DateTools::decodeDate($value);
      }
      catch (\Exception $e) {
        // @todo Handle this.
      }
    }
    if (isset($this->components[$this->name])) {
      return $this->components[$this->name];
    } else {
      return $this->components;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    $this->components[$this->name] = $value;
    // Notify the parent of any changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }

}
